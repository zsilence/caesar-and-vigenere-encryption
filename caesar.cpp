#include <bits/stdc++.h>

#define MOD 26

using namespace std;


string encrypt(string& str, int key) {
    string res(str.length(), 'A');

    for(size_t i = 0; i < str.length(); ++i)
        if(str[i] >= 'A' && str[i] <= 'Z')
	        res[i] += (str[i] + (key % MOD) - 'A') % MOD;
    	else
	        res[i] = str[i];

    return res;
}

string decrypt(string str, int key) {
    string res(str.length(), 'A');

    for(size_t i = 0; i < str.length(); ++i)
        if(str[i] >= 'A' && str[i] <= 'Z')
	        res[i] += (str[i] - (key % MOD) - 'A' + MOD) % MOD;
	    else
	        res[i] = str[i];
    	
    return res;
}

int main() {
    string str;
    int key;
    char todo;    

    string in_file_name;
    string out_file_name;

    cout << "encrypt/decrypt[e/d]: ";
    cin >> todo;
    cout << "enter key(int): ";
    cin >> key;
    cout << "enter input file name: ";
    cin >> in_file_name;
    cout << "enter output file name: ";
    cin >> out_file_name;

    fstream in_file(in_file_name);
    ofstream out_file(out_file_name);

    getline(in_file, str, '\0');
    in_file.close();

    string res;
    
    if(todo == 'e')
        res = encrypt(str, key);
    else if(todo == 'd')
        res = decrypt(str, key);
    else return 0;

    out_file << res;
    out_file.close();
 
    return 0;
}
