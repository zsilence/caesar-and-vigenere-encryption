#include <bits/stdc++.h>

#define MOD 26

using namespace std;


string encrypt(string& str, string& key) {
    string res(str.length(), 'A');
    int k = 0;
    int cnt = 0;

    for(size_t i = 0; i < str.length(); ++i)
        if(str[i] >= 'A' && str[i] <= 'Z') {        
            k = key[cnt] - 'A';
            res[i] += (str[i] + (k % MOD) - 'A') % MOD;
            cnt = (cnt + 1) % key.length();
        } else 
	        res[i] = str[i];            

    return res;
}

string decrypt(string& str, string& key) {
    string res(str.length(), 'A');
    int k = 0;
    int cnt = 0;

    for(size_t i = 0; i < str.length(); ++i)
        if(str[i] >= 'A' && str[i] <= 'Z') {            
            k = key[cnt] - 'A';
            res[i] += (str[i] - (k % MOD) - 'A' + MOD) % MOD;
            cnt = (cnt + 1) % key.length();
        } else 
	        res[i] = str[i];  

    return res;
}

int main() {
    string str;
    string key;
    char todo;    

    string in_file_name;
    string out_file_name;

    cout << "encrypt/decrypt[e/d]: ";
    cin >> todo;
    cout << "enter key(string): ";
    cin >> key;
    cout << "enter input file name: ";
    cin >> in_file_name;
    cout << "enter output file name: ";
    cin >> out_file_name;

    fstream in_file(in_file_name);
    ofstream out_file(out_file_name);

    getline(in_file, str, '\0');
    in_file.close();

    string res;
    
    if(todo == 'e')
        res = encrypt(str, key);
    else if(todo == 'd')
        res = decrypt(str, key);
    else return 0;

    out_file << res;
    out_file.close();
 
    return 0;
}
